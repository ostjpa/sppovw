'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:MaklumatPenggunaHazriCtrl
 * @description
 * # MaklumatPenggunaHazriCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('MaklumatPenggunaHazriCtrl', ['$log', '$q', 'maklumatPenggunaHazriService', function ($log, $q, maklumatPenggunaHazriService) {

    var $ctrl = this;
    $ctrl.pengguna = {};
$ctrl.senaraiPengguna = [];
    // $ctrl.senaraiPengguna = [{ "nama": "Rohaya Yahaya", "emel": "rohaya.yahaya@jpa.gov.my" },
    // { "nama": "Burhanuddin Bin Md Zain", "emel": "burhan.zain@jpa.gov.my" },
    // { "nama": "Wan Nazirul Hafeez Bin Wan Safie", "emel": "Hafeez.safie@jpa.gov.my" }]

    $ctrl.addPengguna = function () {
      /*TODO : INSPECT $ctrl.pengguna*/
      $log.info('$ctrl.pengguna', $ctrl.pengguna);

      maklumatPenggunaHazriService.daftarPengguna($ctrl.pengguna).then(function (resp) {
        $log.info('ini respon data', resp);

        $ctrl.message = resp.data;
        if ($ctrl.message.statusCode == '1') {
          alert("data berjaya disimpan");

        } else {
          return $q.reject($ctrl.message);
        }
      });
    };

    

    $ctrl.getAllPengguna = function () {

      maklumatPenggunaHazriService.SenaraiPengguna().then(function (resp) {

        $log.info('ini senarai pegguna', resp);
        $ctrl.senaraiPengguna = resp.data;

      });
    };

    $ctrl.getAllPengguna();

  }]);
