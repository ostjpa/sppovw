'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:TempahanBilikCtrl
 * @description
 * # TempahanBilikCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('TempahanBilikCtrl'['$log', '$q', 'tempahanBilikService', 'lookupService', function ($log, $q, tempahanBilikService, lookupService) {
    var $ctrl = this;

    /*define object */
    $ctrl.tetamu = {};
    $ctrl.bilik = {};
    $ctrl.senaraiBilik = [];

    $ctrl.addTempahan = function () {

      /*TODO : INSPECT $ctrl.pengguna*/
      $log.info('$ctrl.addTempahan', $ctrl.tetamu);


    };

    $ctrl.bindBilik = function () {

      lookupService.lookupBilik().then(function (resp) {
        /*TODO : INSPECT resp.data*/
        $log.info('resp senaraiBilik', resp.data);
        $ctrl.senaraiBilik = resp.data;
      }, function (err) {
        /*TODO : error*/
        $log.error('senaraiBilik error', err);
      });
    };

    $ctrl.bindBilik();
  }]);
