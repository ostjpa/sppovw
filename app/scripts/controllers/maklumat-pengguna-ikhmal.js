'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:MaklumatPenggunaIkhmalCtrl
 * @description
 * # MaklumatPenggunaIkhmalCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('MaklumatPenggunaIkhmalCtrl', ['$log', '$q', 'manualPenggunaIkhmalService',
    function ($log, $q, manualPenggunaIkhmalService) {
      var $ctrl = this;

      $ctrl.pengguna = {};

      $ctrl.senaraiPengguna = [
        { "nama": "Rohaya Yahaya", "emel": "rohaya.yahaya@jpa.gov.my" },
        { "nama": "Burhanuddin Bin Md Zain", "emel": "burhan.zain@jpa.gov.my" },
        { "nama": "Wan Nazirul Hafeez Bin Wan Safie", "emel": "Hafeez.safie@jpa.gov.my" }
      ];



      $ctrl.daftarPengguna = function () {
        $log.info('daftarPenggunaok');
        manualPenggunaIkhmalService.daftarPengguna($ctrl.pengguna).then(function (resp) {
          $log.info(resp);
          $ctrl.getAllPengguna();
          alert('ok dah');
        }, function (error) {
          $log.error(error)
        });
      };

      $ctrl.getAllPengguna = function () {

        manualPenggunaIkhmalService.getAllPengguna().then(function (resp) {
          $log.info('resp Senarai Penguna', resp);

          $ctrl.senaraiPengguna = resp.data;

        }, function (error) {
          $log.error(error);

        })

      };


      $ctrl.senaraiSkim = [];
      $ctrl.senaraiGred = [];

      /*TODO : dropdown dependency*/
      $ctrl.bindSkim = function () {

        lookupService.lookupSkim().then(function (resp) {
          /*TODO : INSPECT resp.data*/
          $log.info('resp senaraiSkim', resp.data);
          $ctrl.senaraiSkim = resp.data;
        }, function (err) {
          /*TODO : error*/
          $log.error('senaraiPengguna error', err);
        });
      };

      $ctrl.bindGred = function () {

        if ($ctrl.pengguna.skim != null) {
          var paramObj = { 'gred_kod_skim': $ctrl.pengguna.skim.kod };
          lookupService.lookupGred(paramObj).then(function (resp) {
            /*TODO : INSPECT resp.data*/
            $log.info('resp senaraiGred', resp.data);
            $ctrl.senaraiGred = resp.data;
          }, function (err) {
            /*TODO : error*/
            $log.error('senaraiGred error', err);
          });
        } else {
          $ctrl.senaraiGred = [];
        }
      };

      $ctrl.bindSkim();



    }]);
