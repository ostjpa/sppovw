'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:MaklumatPenggunaMizaCtrl
 * @description
 * # MaklumatPenggunaMizaCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('MaklumatPenggunaMizaCtrl', ['$log', '$q', 'maklumatPenggunaMizaService', '$route', function ($log, $q, maklumatPenggunaMizaService, $route) {

    var $ctrl = this;

    /*todo: define object Pengguna */
    $ctrl.pengguna = {};

    /*todo : define object senarai pengguna */
    $ctrl.senaraiPengguna = [{ "nama": "Rohaya Yahaya", "emel": "rohaya.yahaya@jpa.gov.my" },
    { "nama": "Burhanuddin Bin Md Zain", "emel": "burhan.zain@jpa.gov.my" },
    { "nama": "Wan Nazirul Hafeez Bin Wan Safie", "emel": "Hafeez.safie@jpa.gov.my" }]

    $ctrl.addPengguna = function () {

      /*TODO : INSPECT $ctrl.pengguna*/
      $log.info('$ctrl.addPengguna', $ctrl.pengguna);

      /* TODO: inject service dalam method*/
      maklumatPenggunaMizaService.daftarPengguna($ctrl.pengguna).then(function (resp) {
        $log.info('resp daftar', resp);
        $ctrl.message = resp.data;
        if ($ctrl.message.statusCode == '1') {
          alert("data berjaya disimpan");
          /*TODO : clear form data: cara 1 */
          // $ctrl.pengguna = {};

          /*TODO : clear form data: cara 2 validation */
          $route.reload();
        } else {
          return $q.reject($ctrl.message);
        }
      }, function (err) {
        $log.error(err);
      });

    };

    /*TODO : create method getAllPengguna */
    $ctrl.getAllPengguna = function () {
      $log.info('resp senaraiPengguna', $ctrl.senaraiPengguna);
      maklumatPenggunaMizaService.SenaraiPengguna().then(function (resp) {
        $log.info('resp dari database', resp);
        $ctrl.senaraiPengguna = resp.data;
      }, function (err) {
        $log.error(err);
      });

    };

    /*TODO : call function/method */
    $ctrl.getAllPengguna();


  }]);
