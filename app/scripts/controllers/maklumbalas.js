'use strict';

/**
 * @ngdoc function
 * @name spmbTrainingApp.controller:MaklumbalasCtrl
 * @description
 * # MaklumbalasCtrl
 * Controller of the spmbTrainingApp
 */
angular.module('sppovwApp')
    .controller('MaklumbalasCtrl', ['maklumbalasService', '$log', '$mdDialog', function (maklumbalasService, $log, $mdDialog) {
        var $ctrl = this;

        // $ctrl.soalan_array = maklumbalasService.getSoalan();
        $ctrl.soalan_array = [];
        $ctrl.maklumbalas = {};
        $ctrl.maklumbalas.jawapan_array = [];
        $ctrl.maklumbalas.kursus_id = 1;
        //$ctrl.maklumbalas.peserta_id = '790129105655';
        $ctrl.total = 0;
        $ctrl.viewSurvey = false;

        var populateSoalan = function () {

            maklumbalasService.getSoalan().then(function (resp) {
                //$log.info('getSoalan',resp);
                $ctrl.soalan_array = resp.data;
            }).then(function () {
                angular.forEach($ctrl.soalan_array, function (value, key) {
                    // console.log(value, key);
                    $ctrl.maklumbalas.jawapan_array.push({
                        soalan_id: value.soalan_id,
                        jawapan: null
                    });
                });
            });
        };


        $ctrl.getAnsweredTotal = function () {
            $ctrl.total = 0;
            angular.forEach($ctrl.maklumbalas.jawapan_array, function (value, key) {
                // console.log(value, key);
                if (value.jawapan !== null && value.jawapan.length !== 0) {
                    $ctrl.total += 1;
                }
            });
            $ctrl.unansweredQuestionNo();
            return $ctrl.total;
        };

        $ctrl.unansweredQuestionNo = function () {
            $ctrl.questionNoArray = [];

            angular.forEach($ctrl.maklumbalas.jawapan_array, function (value, key) {
                // console.log(value, key);
                if (value.jawapan === null) {
                    $ctrl.questionNoArray.push(key + 1);
                }
            });

            if ($ctrl.questionNoArray.length > 0) {
                return "Soalan " + $ctrl.questionNoArray.join(', ') + " masih belum dijawab.";
            } else {
                return "Semua soalan selesai dijawab.";
            }
        };

        $ctrl.hantarMaklumBalas = function () {
            //$log.info('hantar maklumbalas',$ctrl.maklumbalas);
            maklumbalasService.hantarMaklumbalas($ctrl.maklumbalas).then(function (resp) {
                $ctrl.message = resp.data;
                $ctrl.showAlert();
                $ctrl.btnHantarDisable = true;

            }, function (err) {
                $log.error('hantar maklumbalas error', err);
            });
        };
                        
        $ctrl.nextStep = function () {
            $ctrl.viewSurvey = true;
            populateSoalan();
        };

        $ctrl.showAlert = function (ev) {

            var confirm = $mdDialog.confirm()
            //.title('Would you like to delete your debt?')
                .textContent('Terima kasih kerana menghantar maklumbalas ini.')
                .ariaLabel('Alert Dialog Demo')
                //.targetEvent(ev)
                .ok('OK');
            //.cancel('Sounds like a scam');

            $mdDialog.show(confirm).then(function () {
                $ctrl.maklumbalas = {};
                $ctrl.maklumbalas.jawapan_array = [];
                $ctrl.maklumbalas.kursus_id = 1;
                $ctrl.total = 0;
                $ctrl.viewSurvey = false;
            }, function () {
            });
        };

    }]);
