'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.tempahanBilikService
 * @description
 * # tempahanBilikService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('tempahanBilikService', function () {
    return {
        senaraiPegawai : function(pegawai) {
            return $http.get('./api/pegawai/get-info-pegawai.php', pegawai);
        },
        senaraiBilik : function() {
            return $http.get('./api/bilik/get-senarai-bilik.php');
        },
        daftarBilik : function() {
            return $http.post('./api/bilik/tempah-bilik.php');
        },
    };
  
  });
