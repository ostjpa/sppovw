'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.localstorageService
 * @description
 * # localstorageService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
    .factory('localstorageService', function() {
        var pengguna = [];
        return {
            getAllPengguna: function() {
                var penggunaTmp = [];
                penggunaTmp = localStorage['SPPOVW.Pengguna'] ? JSON.parse(localStorage['SPPOVW.Pengguna']) : [];
                pengguna = penggunaTmp;
                return pengguna;
            },
            addPengguna: function(penggunaBaru) {
                pengguna.push(penggunaBaru);
                localStorage['SPPOVW.Pengguna'] = JSON.stringify(pengguna);
            },
            clearAllPengguna: function() {
                delete localStorage['SPPOVW.Pengguna'];
            }
        };
    });