'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.maklumatPenggunaIkhmalService
 * @description
 * # maklumatPenggunaIkhmalService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('maklumatPenggunaIkhmalService', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
