'use strict';

/**
 * @ngdoc service
 * @name viewmaklumatPenggunaAnaApp.maklumatPenggunaAnaService
 * @description
 * # maklumatPenggunaAnaService
 * Factory in the viewmaklumatPenggunaAnaApp.
 */
angular.module('sppovwApp')
  .factory('maklumatPenggunaAnaService', ['$http', function ($http) {


    // Public API here
    return {
      daftarPengguna: function (penggunaObj) {
        // var config = {
        //   method: 'POST',
        //   url: './api/training/insert-api.php',
        //   params: penggunaObj
        // };
        return $http.post('./api/training/insert-api.php',penggunaObj);
      },
    
    senaraiPengguna : function (penggunaObj) {
            return $http.get('./api/training/select-api.php');
        }
    };
  }]);

