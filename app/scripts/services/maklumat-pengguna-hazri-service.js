'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.maklumatPenggunaHazriService
 * @description
 * # maklumatPenggunaHazriService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('maklumatPenggunaHazriService', ['$http', function ($http) {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      daftarPengguna: function (penggunaObj) {
        return $http.post('./api/training/insert-api.php', penggunaObj);
      },
      SenaraiPengguna: function () {
        return $http.get('./api/training/select-api.php');
      }
    };
  }]);
