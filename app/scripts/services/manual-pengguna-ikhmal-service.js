'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.manualPenggunaIkhmalService
 * @description
 * # manualPenggunaIkhmalService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('manualPenggunaIkhmalService', ['$http', function ($http) {


    // Public API here
    return {
      daftarPengguna: function (penggunaObj) {

        return $http.post('./api/training/insert-apiMandryn.php', penggunaObj);
      },
       getAllPengguna: function () {

        return $http.get('./api/training/select-api.php');
      }

    };
  }]);
