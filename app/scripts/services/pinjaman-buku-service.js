'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.pinjamanBukuService
 * @description
 * # pinjamanBukuService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('pinjamanBukuService', function () {

    return {
      getPeribadi: function (peribadi) {
        return $http.get('./api/pegawai/get-info-pegawai.php', peribadi);
      },
      addBuku: function (buku) {
        return $http.post('./api/buku/pinjam-buku.php', buku);
      },
      lookupBuku: function () {
        return $http.get('./api/buku/get-senarai-buku.php');
      }
    };
  });
