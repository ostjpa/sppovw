'use strict';

/**
 * @ngdoc service
 * @name spmbTrainingApp.maklumbalasService
 * @description
 * # maklumbalasService
 * Factory in the spmbTrainingApp.
 */
angular.module('sppovwApp')
    .factory('maklumbalasService', function ($timeout, $http) {

        return {

            getSoalan: function () {
                /*var soalan=[{
                 "soalan_id": 1,
                 "soalan": "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.",
                 "maklumbalas_id": 21
                 }, {
                 "soalan_id": 2,
                 "soalan": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.",
                 "maklumbalas_id": 36
                 }, {
                 "soalan_id": 3,
                 "soalan": "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.",
                 "maklumbalas_id": 8
                 }, {
                 "soalan_id": 4,
                 "soalan": "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.",
                 "maklumbalas_id": 34
                 }, {
                 "soalan_id": 5,
                 "soalan": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
                 "maklumbalas_id": 61
                 }, {
                 "soalan_id": 6,
                 "soalan": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
                 "maklumbalas_id": 49
                 }, {
                 "soalan_id": 7,
                 "soalan": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
                 "maklumbalas_id": 30
                 }, {
                 "soalan_id": 8,
                 "soalan": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                 "maklumbalas_id": 44
                 }, {
                 "soalan_id": 9,
                 "soalan": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
                 "maklumbalas_id": 69
                 }, {
                 "soalan_id": 10,
                 "soalan": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.",
                 "maklumbalas_id": 56
                 }];

                 return $timeout(function() {
                 return {
                 data: [{
                 "soalan_id": 10,
                 "soalan": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.",
                 "maklumbalas_id": 56
                 }]
                 };
                 }, 500);*/

                return $http.get('./api/maklumbalas/senarai-soalan.php');
            },
            getSenaraiSelesai: function () {
                return $http.get('./api/maklumbalas/maklumbalas-selesai.php');
            },
            hantarMaklumbalas: function (maklumbalas) {
                /*return $timeout(function() {
                 return {
                 data: maklumbalas
                 };
                 }, 500);*/

                return $http.post('./api/maklumbalas/hantar_maklumbalas.php', maklumbalas);
            }
        };
    });
