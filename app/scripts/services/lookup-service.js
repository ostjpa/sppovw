'use strict';
/**
 * @ngdoc service
 * @name sppovwApp.lookupService
 * @description
 * # lookupService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
    .factory('lookupService', ['$http', function ($http) {
        return {
            lookupUnit: function () {
                return $http.get('./api/pegawai/lookup-unit.php');
            },
            lookupGred: function (paramsObj) {
                var config = {
                    method: 'GET',
                    url: './api/training/training-lookupgred.php',
                    params: paramsObj
                };
                return $http(config);
            },
            lookupSkim: function () {
                return $http.get('./api/training/training-lookupskim.php');
            }
        };
    }]);