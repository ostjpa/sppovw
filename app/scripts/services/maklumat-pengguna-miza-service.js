'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.maklumatPenggunaMizaService
 * @description
 * # maklumatPenggunaMizaService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('maklumatPenggunaMizaService', ['$http', function ($http) {

    // Public API here
    return {
      daftarPengguna: function (penggunaObj) {
                return $http.post('./api/training/insert-api.php', penggunaObj);
      },
      SenaraiPengguna: function () {
                return $http.get('./api/training/select-api.php');
      }


    };
  }]);
