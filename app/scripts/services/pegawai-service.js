'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.pegawaiService
 * @description
 * # pegawaiService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('pegawaiService',['$http', function ($http) {
    return {
        daftarPegawai : function(pegawai) {
            return $http.post('./api/pegawai/daftar_pegawai.php', pegawai);
        },
        senaraiPegawai : function() {
            return $http.get('./api/pegawai/senarai-pegawai.php');
        }
    };
  }]);
