'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.maklumatPenggunaService
 * @description
 * # maklumatPenggunaService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('maklumatPenggunaService', ['$http', function ($http) {
      return {
          /*TODO : STEP 5 : Create service*/
          daftarPengguna : function(penggunaObj) {
              return $http.post('./api/training/insert-api.php', penggunaObj);
          },
          /* TODO : STEP 5 : retrieve record without parameter */
          senaraiPengguna : function() {
              return $http.get('./api/training/select-api.php');
          },
          /* TODO : STEP 5 : retrieve record with parameter */
          // senaraiPengguna : function(paramsObj) {
          //     var config = {
          //         method:'GET',
          //         url:'./api/training/select-api.php',
          //         params:paramsObj
          //     };
          //     return $http(config);
          // }
      };
  }]);
