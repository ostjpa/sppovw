'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.perlepasanKontrakService
 * @description
 * # perlepasanKontrakService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('perlepasanKontrakService', ['$http', function ($http) {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      daftarPengguna: function (penggunaObj) {
        return $http.post('./api/pegawai/lepas-kontrak.php', penggunaObj);
      },
      getinfopegawai: function (param) {
        return $http.get('./api/pegawai/get-info-pegawai.php',param );
      }
    };
  }]);
