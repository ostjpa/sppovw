'use strict';

/**
 * @ngdoc overview
 * @name sppovwApp
 * @description
 * # sppovwApp
 *
 * Main module of the application.
 */
angular
    .module('sppovwApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngMaterial',
        'ui.bootstrap',
        'ngMessages'
    ])
    .config(function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                redirectTo: '/maklumbalas'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/daftar-pegawai', {
                templateUrl: 'views/daftar-pegawai.html',
                controller: 'DaftarPegawaiCtrl',
                controllerAs: 'daftarPegawai'
            })
            .when('/maklumbalas', {
                templateUrl: 'views/maklumbalas.html'
            })
            .when('/maklumbalas-selesai', {
                templateUrl: 'views/maklumbalas-selesai.html'
            })
            .when('/md1', {
                templateUrl: 'views/md1.html',
                controller: 'Md1Ctrl',
                controllerAs: 'md1'
            })
            .when('/test-localstorage', {
                templateUrl: 'views/test-localstorage.html'
            })
            .when('/maklumat-pengguna', {
                templateUrl: 'views/maklumat-pengguna.html'
            })
            .when('/perlepasan-kontrak', {
                templateUrl: 'views/perlepasan-kontrak.html'
            })
            .when('/pinjaman-buku', {
                templateUrl: 'views/pinjaman-buku.html'
            })
            .when('/tempahan-bilik', {
                templateUrl: 'views/tempahan-bilik.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    });