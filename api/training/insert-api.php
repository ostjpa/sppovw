<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : RETRIEVES INPUT PARAMETER */
$inputObj = json_decode(file_get_contents("php://input"));

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sql = <<<SQL
INSERT INTO `maklumbalas`.`pengguna`
            (`IDpengguna`,
             `nama`,
             `emel`)
VALUES ('$inputObj->IDpengguna',
		'$inputObj->nama',
		'$inputObj->emel')
SQL;

/* TODO : INSPECT SQL */
//echo $sql;exit;

/* TODO : INITIALIZE DB COMMAND OBJECT */
$DBCommandObj = new DBCommand($DBQueryObj);

/* TODO : CONFIGURE DB COMMAND OBJECT */
$DBCommandObj->executeCustomQueryCommand($sql);

/* TODO : CREATE RESPONSE STATUS OBJECT */
$responseObj = new MagicObject();

/* TODO : CHECK DB OPERATION STATUS */
if($DBCommandObj->getAffectedRowCount()!= -1){
    $responseObj->status ='OK';
    $responseObj->statusCode ='1';
}else{
    $responseObj->status ='FAILED';
    $responseObj->statusCode = $DBCommandObj->getAffectedRowCount();
}

/* TODO : DISPLAY STATUS IN JSON */
echo $responseObj->getJsonString();