'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:MaklumbalasSelesaiCtrl
 * @description
 * # MaklumbalasSelesaiCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('MaklumbalasSelesaiCtrl',['maklumbalasService', '$log', '$mdDialog', function (maklumbalasService, $log, $mdDialog) {

      var $ctrl = this;
      $ctrl.senaraiSelesai =[];

      $ctrl.getSenaraiSelesai = function () {

          maklumbalasService.getSenaraiSelesai().then(function (resp) {
              $log.info('Senarai Selesai',resp);
              $ctrl.senaraiSelesai = resp.data;
          }, function (err) {
              $log.error('hantar maklumbalas error', err);
          });
      };
      $ctrl.getSenaraiSelesai();
  }]);
