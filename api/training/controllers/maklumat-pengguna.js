'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:MaklumatPenggunaCtrl
 * @description
 * # MaklumatPenggunaCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
    .controller('MaklumatPenggunaCtrl', ['$log', '$q', 'maklumatPenggunaService', '$uibModal', 'lookupService', function ($log, $q, maklumatPenggunaService, $uibModal, lookupService) {

        /*TODO : Step 4 : define $ctrl*/
        var $ctrl = this;

        /*TODO : Step 4 :  define object*/
        $ctrl.pengguna = {};
        $ctrl.senaraiPengguna = [];
        $ctrl.senaraiSkim = [];
        $ctrl.senaraiGred = [];

        /*TODO : Step 4 : create method/function addPengguna*/
        $ctrl.addPengguna = function () {

            /*TODO : INSPECT $ctrl.pengguna*/
            $log.info('$ctrl.addPengguna', $ctrl.pengguna);

            /*TODO : Step 5.1 : inject service dalam method/function "$ctrl.addPengguna"*/
            /*maklumatPenggunaService.daftarPengguna($ctrl.pengguna).then(function (resp) {
                /!*TODO : INSPECT resp.data*!/
                $log.info('resp daftarPengguna',resp.data);
                $ctrl.message = resp.data;
  
                if($ctrl.message.statusCode=='1'){
                    /!*TODO : success message*!/
                    alert($ctrl.message.status);
                }else{
                    /!*TODO : error message*!/
                    return $q.reject($ctrl.message);
                }
            }, function (err) {
                /!*TODO : error*!/
                $log.error('daftarPengguna error', err);
            });*/
            /* END Step 5.1*/
        };

        /*TODO : Step 4 : create method/function getAllPengguna*/
        $ctrl.getAllPengguna = function () {

            /*TODO : Step 5.1 : inject service dalam method/function "$ctrl.getAllPengguna"*/
            maklumatPenggunaService.senaraiPengguna().then(function (resp) {
                /*TODO : INSPECT resp.data*/
                $log.info('resp senaraiPengguna', resp.data);
                $ctrl.senaraiPengguna = resp.data;
            }, function (err) {
                /*TODO : error*/
                $log.error('senaraiPengguna error', err);
            });
            /* END Step 5.1*/

        };

        /*TODO : Step 6 : dropdown dependency*/
        $ctrl.bindSkim = function () {

            lookupService.lookupSkim().then(function (resp) {
                /*TODO : INSPECT resp.data*/
                $log.info('resp senaraiSkim', resp.data);
                $ctrl.senaraiSkim = resp.data;
            }, function (err) {
                /*TODO : error*/
                $log.error('senaraiPengguna error', err);
            });
        };

        $ctrl.bindGred = function () {

            if ($ctrl.pengguna.skim != null) {
                var paramObj = { 'gred_kod_skim': $ctrl.pengguna.skim.kod };
                lookupService.lookupGred(paramObj).then(function (resp) {
                    /*TODO : INSPECT resp.data*/
                    $log.info('resp senaraiGred', resp.data);
                    $ctrl.senaraiGred = resp.data;
                }, function (err) {
                    /*TODO : error*/
                    $log.error('senaraiGred error', err);
                });
            } else {
                $ctrl.senaraiGred = [];
            }
        };


        /*TODO : Step 7 : Validation (views/maklumat-pengguna.html)*/


        /*TODO : Step 8 : Angular UI Component (Modal Box)*/
        $ctrl.items = ['item1', 'item2', 'item3'];

        $ctrl.animationsEnabled = true;
        $ctrl.open = function () {
            var modalInstance = $uibModal.open({
                animation: $ctrl.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                size: 'lg',
                resolve: {
                    items: function () {
                        return $ctrl.senaraiPengguna;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $ctrl.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


        /*TODO : call function/method*/
        $ctrl.getAllPengguna();
        $ctrl.bindSkim();

    }]);
