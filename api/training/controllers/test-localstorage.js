'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:TestLocalstorageCtrl
 * @description
 * # TestLocalstorageCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
    .controller('TestLocalstorageCtrl', function(localstorageService) {
        var ctrl = this;

        ctrl.pengguna = {};
        ctrl.senaraiPengguna = [];

        var getAllPengguna = function() {
            ctrl.senaraiPengguna = localstorageService.getAllPengguna();
        };

        ctrl.addPengguna = function() {
            localstorageService.addPengguna(angular.copy(ctrl.pengguna));
            ctrl.pengguna = {};
            getAllPengguna();
        };

        ctrl.clearAllPengguna = function() {
            localstorageService.clearAllPengguna();
            getAllPengguna();
        };

        getAllPengguna();
    });