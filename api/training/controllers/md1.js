'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:Md1Ctrl
 * @description
 * # Md1Ctrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('Md1Ctrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
