'use strict';

/**
 * @ngdoc function
 * @name viewmaklumatPenggunaAnaApp.controller:MaklumatPenggunaAnaCtrl
 * @description
 * # MaklumatPenggunaAnaCtrl
 * Controller of the viewmaklumatPenggunaAnaApp
 */
angular.module('sppovwApp')
  .controller('MaklumatPenggunaAnaCtrl', ['$log', '$q','maklumatPenggunaAnaService','$route', function ($log, $q, maklumatPenggunaAnaService,$route) {
    var $ctrl = this;
    /*todo:define object pengguna*/
    $ctrl.pengguna = {};

    $ctrl.senaraiPengguna = [{ "nama": "Rohaya Yahaya", "emel": "rohaya.yahaya@jpa.gov.my" },
    { "nama": "Burhanuddin Bin Md Zain", "emel": "burhan.zain@jpa.gov.my" },
    { "nama": "Wan Nazirul Hafeez Bin Wan Safie", "emel": "Hafeez.safie@jpa.gov.my" }]

    $ctrl.addPengguna = function () {

      /*TODO : INSPECT $ctrl.pengguna*/
      $log.info('$ctrl.addPengguna', $ctrl.pengguna);

      maklumatPenggunaAnaService.daftarPengguna($ctrl.pengguna).then(function(resp){
         $log.info('resp daftar', resp);

         $ctrl.message= resp.data;
         if($ctrl.message.statusCode=='1'){
           alert("data berjaya disimpan");
           //$ctrl.pengguna ={}; //cara 1

           $route.reload();//cara 2
         }else {
           return $q.reject($ctrl.message);
         }
      });
    };
    $ctrl.getAllPengguna = function () {
      /*TODO : INSPECT $ctrl.pengguna*/

       //$log.info('$ctrl.addPengguna', $ctrl.pengguna);

      maklumatPenggunaAnaService.senaraiPengguna().then(function(resp){
         $log.info('resp data dari database', resp);
         $ctrl.senaraiPengguna =resp.data;
      
      },function(err){
        $log.error(err);
     
    });

};
      /*TODO : call function/method*/
      $ctrl.getAllPengguna();


  


  }]);
