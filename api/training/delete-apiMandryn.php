<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : RETRIEVES INPUT PARAMETER */
$inputObj = json_decode(file_get_contents("php://input"));

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sqlCmdObj = new DBCommand($DBQueryObj);
$sqlCmdObj->setDELETEfromTable('pengguna');
$sqlCmdObj->addConditionStatement('IDpengguna',$inputObj->IDpengguna,IFieldType::STRING_TYPE,IConditionOperator::NONE);

/* TODO : INSPECT SQL */
//echo $sqlCmdObj->getSQLstring();exit;

/* TODO : QUERY DATABASE */
$sqlCmdObj->executeQueryCommand();

/* TODO : CREATE RESPONSE STATUS OBJECT */
$responseObj = new MagicObject();

/* TODO : CHECK DB OPERATION STATUS */
if($sqlCmdObj->getAffectedRowCount()> 0){
    $responseObj->status ='OK';
    $responseObj->statusCode ='1';
}else{
    $responseObj->status ='FAILED';
    $responseObj->statusCode = $sqlCmdObj->getAffectedRowCount();
}

/* TODO : DISPLAY STATUS IN JSON */
echo $responseObj->getJsonString();