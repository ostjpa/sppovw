<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : DEFINE VALUE */
$conditionStr='';

/* TODO : RETRIEVES INPUT PARAMETER */
if (count($_GET)>0) {

    $pagingObj = (object) $_GET;
    unset($_GET);

    /* Retrieve params if any */
    if(isset($pagingObj->gred_kod_skim)){
        if($conditionStr===''){
            $conditionStr=' WHERE ';
            $conditionStr.='gred_kod_skim =\''. mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->gred_kod_skim).'\'';
        }else{
            $conditionStr.=' AND gred_kod_skim =\'' . mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->gred_kod_skim).'\'';
        }
    }
}

/* TODO : CONSTRUCT SQL */
$sql = <<<SQL
SELECT gred_kod, gred_perihal
FROM _kod_gred
$conditionStr
LIMIT 0, 20
SQL;

/* TODO : INSPECT SQL */
//echo $sql;exit;

/* TODO : QUERY DATABASE */
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

/* TODO : CONVERT RECORDSET TO JSON */
echo $DBQueryObj->getRowsInJSON();
