<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sqlQuery = new SQLQuery();
$sqlQuery->setSELECTQuery('lookup_buku');
$sqlQuery->addReturnField('buku_kod');
$sqlQuery->addReturnField('buku_judul');

/* TODO : RETRIEVES INPUT PARAMETER IF ANY*/
if (count($_GET)>0) {

    $pagingObj = (object) $_GET;
    unset($_GET);

    /* Retrieve params if any */
    if(isset($pagingObj->buku_kod)){
        $sqlQuery->addLikeConditionField('buku_kod','%'.mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->buku_kod).'%',IConditionOperator::NONE);
    }
    if(isset($pagingObj->buku_judul)){
        $sqlQuery->addLikeConditionField('buku_judul',mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->buku_judul),IConditionOperator::AND_OPERATOR);
    }
}
/* TODO : INSPECT SQL */
//echo $sqlQuery->getSQLQuery();exit;

/* TODO : QUERY DATABASE */
$DBQueryObj->setSQL_Statement($sqlQuery->getSQLQuery());
$DBQueryObj->runSQL_Query();

/* TODO : CONVERT RECORDSET TO JSON */
echo $DBQueryObj->getRowsInJSON();
