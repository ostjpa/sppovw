<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('./../../vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('./../config/db_connection.php');

/* TODO : RETRIEVES INPUT PARAMETER */
$inputObj = json_decode(file_get_contents("php://input"));

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sqlCmdObj = new DBCommand($DBQueryObj);
$sqlCmdObj->setINSERTintoTable('tempahan_bilik');
$sqlCmdObj->addINSERTcolumn('bilik_id',$inputObj->bilik_id,IFieldType::INTEGER_TYPE);
$sqlCmdObj->addINSERTcolumn('kegunaan',$inputObj->kegunaan,IFieldType::STRING_TYPE);
$sqlCmdObj->addINSERTcolumn('peribadi_nokp',$inputObj->peribadi_nokp,IFieldType::STRING_TYPE);
$sqlCmdObj->addINSERTcolumn('tempahan_id',$inputObj->tempahan_id,IFieldType::INTEGER_TYPE);
$sqlCmdObj->addINSERTcolumn('tkh_tempah',$inputObj->tkh_tempah,IFieldType::DATETIME_TYPE);

//echo $sqlCmdObj->getSQLstring();exit;
$sqlCmdObj->executeQueryCommand();

/* TODO : CREATE RESPONSE STATUS OBJECT */
$responseObj = new MagicObject();

/* TODO : CHECK DB OPERATION STATUS */
if($sqlCmdObj->getAffectedRowCount()!= -1){
    $responseObj->status ='OK';
    $responseObj->statusCode ='1';
}else{
    $responseObj->status ='FAILED';
    $responseObj->statusCode = $sqlCmdObj->getAffectedRowCount();
}

/* TODO : DISPLAY STATUS IN JSON */
echo $responseObj->getJsonString();