<?php
/* TODO : INCLUDE VENDOR LIBRARY */
require_once('../../../sppovw/vendor/autoload.php');

/* TODO : INCLUDE DATABASE CONFIGURATION */
require_once ('../config/db_connection.php');

/* TODO : INITIALIZE DB CONNECTION OBJECT */
$DBQueryObj = new DBQuery($host,$username,$password,$database_name);

/* TODO : CONSTRUCT SQL */
$sqlQuery = new SQLQuery();
$sqlQuery->setSELECTQuery('peribadi');
$sqlQuery->addReturnField('peribadi_nama');
$sqlQuery->addReturnField('peribadi_nokp');

/* TODO : RETRIEVES INPUT PARAMETER IF ANY*/
if (count($_GET)>0) {

    $pagingObj = (object) $_GET;
    unset($_GET);

    /* Retrieve params if any */
    if(isset($pagingObj->peribadi_nama)){
        $sqlQuery->addLikeConditionField('peribadi_nama','%'.mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->peribadi_nama).'%',IConditionOperator::NONE);
    }
    if(isset($pagingObj->peribadi_nokp)){
        $sqlQuery->addLikeConditionField('peribadi_nokp',mysqli_real_escape_string($DBQueryObj->getLink(),$pagingObj->peribadi_nokp),IConditionOperator::AND_OPERATOR);
    }
}
/* TODO : INSPECT SQL */
//echo $sqlQuery->getSQLQuery();exit;

/* TODO : QUERY DATABASE */
$DBQueryObj->setSQL_Statement($sqlQuery->getSQLQuery());
$DBQueryObj->runSQL_Query();

/* TODO : CONVERT RECORDSET TO JSON */
echo $DBQueryObj->getRowsInJSON();