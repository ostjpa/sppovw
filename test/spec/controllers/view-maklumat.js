'use strict';

describe('Controller: ViewMaklumatCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var ViewMaklumatCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ViewMaklumatCtrl = $controller('ViewMaklumatCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ViewMaklumatCtrl.awesomeThings.length).toBe(3);
  });
});
