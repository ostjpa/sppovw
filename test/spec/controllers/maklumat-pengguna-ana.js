'use strict';

describe('Controller: MaklumatPenggunaAnaCtrl', function () {

  // load the controller's module
  beforeEach(module('viewmaklumatPenggunaAnaApp'));

  var MaklumatPenggunaAnaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumatPenggunaAnaCtrl = $controller('MaklumatPenggunaAnaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumatPenggunaAnaCtrl.awesomeThings.length).toBe(3);
  });
});
