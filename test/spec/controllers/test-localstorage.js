'use strict';

describe('Controller: TestLocalstorageCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var TestLocalstorageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TestLocalstorageCtrl = $controller('TestLocalstorageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TestLocalstorageCtrl.awesomeThings.length).toBe(3);
  });
});
