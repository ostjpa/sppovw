'use strict';

describe('Controller: MaklumbalasCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var MaklumbalasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumbalasCtrl = $controller('MaklumbalasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumbalasCtrl.awesomeThings.length).toBe(3);
  });
});
