'use strict';

describe('Controller: Md1Ctrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var Md1Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Md1Ctrl = $controller('Md1Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Md1Ctrl.awesomeThings.length).toBe(3);
  });
});
