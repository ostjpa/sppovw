'use strict';

describe('Controller: MaklumatPenggunaMizaCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var MaklumatPenggunaMizaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumatPenggunaMizaCtrl = $controller('MaklumatPenggunaMizaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumatPenggunaMizaCtrl.awesomeThings.length).toBe(3);
  });
});
