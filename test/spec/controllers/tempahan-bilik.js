'use strict';

describe('Controller: TempahanBilikCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var TempahanBilikCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TempahanBilikCtrl = $controller('TempahanBilikCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TempahanBilikCtrl.awesomeThings.length).toBe(3);
  });
});
