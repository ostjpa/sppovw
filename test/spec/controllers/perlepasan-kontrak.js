'use strict';

describe('Controller: PerlepasanKontrakCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var PerlepasanKontrakCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PerlepasanKontrakCtrl = $controller('PerlepasanKontrakCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PerlepasanKontrakCtrl.awesomeThings.length).toBe(3);
  });
});
