'use strict';

describe('Controller: MaklumatPenggunaIkhmalCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var MaklumatPenggunaIkhmalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumatPenggunaIkhmalCtrl = $controller('MaklumatPenggunaIkhmalCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumatPenggunaIkhmalCtrl.awesomeThings.length).toBe(3);
  });
});
