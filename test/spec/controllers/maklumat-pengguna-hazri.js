'use strict';

describe('Controller: MaklumatPenggunaHazriCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var MaklumatPenggunaHazriCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumatPenggunaHazriCtrl = $controller('MaklumatPenggunaHazriCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumatPenggunaHazriCtrl.awesomeThings.length).toBe(3);
  });
});
