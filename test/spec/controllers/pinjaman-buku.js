'use strict';

describe('Controller: PinjamanBukuCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var PinjamanBukuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PinjamanBukuCtrl = $controller('PinjamanBukuCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PinjamanBukuCtrl.awesomeThings.length).toBe(3);
  });
});
