'use strict';

describe('Controller: MaklumbalasSelesaiCtrl', function () {

  // load the controller's module
  beforeEach(module('sppovwApp'));

  var MaklumbalasSelesaiCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumbalasSelesaiCtrl = $controller('MaklumbalasSelesaiCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumbalasSelesaiCtrl.awesomeThings.length).toBe(3);
  });
});
