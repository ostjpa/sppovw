'use strict';

describe('Service: perlepasanKontrakService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var perlepasanKontrakService;
  beforeEach(inject(function (_perlepasanKontrakService_) {
    perlepasanKontrakService = _perlepasanKontrakService_;
  }));

  it('should do something', function () {
    expect(!!perlepasanKontrakService).toBe(true);
  });

});
