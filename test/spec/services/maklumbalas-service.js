'use strict';

describe('Service: maklumbalasService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var maklumbalasService;
  beforeEach(inject(function (_maklumbalasService_) {
    maklumbalasService = _maklumbalasService_;
  }));

  it('should do something', function () {
    expect(!!maklumbalasService).toBe(true);
  });

});
