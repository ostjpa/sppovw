'use strict';

describe('Service: maklumatPenggunaIkhmalService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var maklumatPenggunaIkhmalService;
  beforeEach(inject(function (_maklumatPenggunaIkhmalService_) {
    maklumatPenggunaIkhmalService = _maklumatPenggunaIkhmalService_;
  }));

  it('should do something', function () {
    expect(!!maklumatPenggunaIkhmalService).toBe(true);
  });

});
