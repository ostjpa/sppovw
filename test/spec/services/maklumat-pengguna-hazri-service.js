'use strict';

describe('Service: maklumatPenggunaHazriService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var maklumatPenggunaHazriService;
  beforeEach(inject(function (_maklumatPenggunaHazriService_) {
    maklumatPenggunaHazriService = _maklumatPenggunaHazriService_;
  }));

  it('should do something', function () {
    expect(!!maklumatPenggunaHazriService).toBe(true);
  });

});
