'use strict';

describe('Service: maklumatPenggunaMizaService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var maklumatPenggunaMizaService;
  beforeEach(inject(function (_maklumatPenggunaMizaService_) {
    maklumatPenggunaMizaService = _maklumatPenggunaMizaService_;
  }));

  it('should do something', function () {
    expect(!!maklumatPenggunaMizaService).toBe(true);
  });

});
