'use strict';

describe('Service: maklumatPenggunaAnaService', function () {

  // load the service's module
  beforeEach(module('viewmaklumatPenggunaAnaApp'));

  // instantiate service
  var maklumatPenggunaAnaService;
  beforeEach(inject(function (_maklumatPenggunaAnaService_) {
    maklumatPenggunaAnaService = _maklumatPenggunaAnaService_;
  }));

  it('should do something', function () {
    expect(!!maklumatPenggunaAnaService).toBe(true);
  });

});
