'use strict';

describe('Service: tempahanBilikService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var tempahanBilikService;
  beforeEach(inject(function (_tempahanBilikService_) {
    tempahanBilikService = _tempahanBilikService_;
  }));

  it('should do something', function () {
    expect(!!tempahanBilikService).toBe(true);
  });

});
