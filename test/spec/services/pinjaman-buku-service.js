'use strict';

describe('Service: pinjamanBukuService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var pinjamanBukuService;
  beforeEach(inject(function (_pinjamanBukuService_) {
    pinjamanBukuService = _pinjamanBukuService_;
  }));

  it('should do something', function () {
    expect(!!pinjamanBukuService).toBe(true);
  });

});
