'use strict';

describe('Service: localstorageService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var localstorageService;
  beforeEach(inject(function (_localstorageService_) {
    localstorageService = _localstorageService_;
  }));

  it('should do something', function () {
    expect(!!localstorageService).toBe(true);
  });

});
