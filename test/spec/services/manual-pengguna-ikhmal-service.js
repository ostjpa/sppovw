'use strict';

describe('Service: manualPenggunaIkhmalService', function () {

  // load the service's module
  beforeEach(module('sppovwApp'));

  // instantiate service
  var manualPenggunaIkhmalService;
  beforeEach(inject(function (_manualPenggunaIkhmalService_) {
    manualPenggunaIkhmalService = _manualPenggunaIkhmalService_;
  }));

  it('should do something', function () {
    expect(!!manualPenggunaIkhmalService).toBe(true);
  });

});
